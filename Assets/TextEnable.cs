﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextEnable : MonoBehaviour {

    [SerializeField]
    private Text testText;

    private float timer;
    
	void Update ()
    {
        ToggleText();
        RunToggleTimer();

    }

    //shitty on and off methods
    void ToggleText()
    {
        if (timer <= 0)
        {
            if (testText.enabled)
            {
                testText.enabled = false;

                timer = 1f;
            }
            else if (!testText.enabled)
            {
                testText.enabled = true;

                timer = 1f;
            }           
        }
    }

    void RunToggleTimer()
    {
        timer -= Time.deltaTime;
    }
}
